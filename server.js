const fs = require('fs');
const express = require('express');
require('./databse/db');
require('dotenv').config();
 const app = express();
 const port = process.env.PORT || "3000";
 const swaggerUi = require("swagger-ui-express");
 const swaggerJSDoc = require("swagger-jsdoc");
 app.use(express.json())
 app.use(express.urlencoded({extended:true}))

 const userRouter =  require('./router/userRouter');
 const adminRouter =  require('./router/adminRouter');
 const companyRouter = require('./router/addCompanyRouter');
 const productRouter = require('./router/addProductRouter')
 const staticRouter =  require('./router/staticRouter')
 const userManagementRouter = require('./router/userManagementRouter')
 const customerRouter = require('./router/customerAddRouter')
 const tcRouter =  require('./router/tcRouter')
 const previewRouter = require('./router/previewRouter')
 const qutation=require('./router/qutation')
 const pdf = require('./router/pdfRouter')
 

 app.use('/user',userRouter)
 app.use('/admin',adminRouter)     
 app.use('/company', companyRouter)
 app.use('/product', productRouter)
 app.use('/admin', staticRouter)
 app.use('/userManagement', userManagementRouter)
 app.use('/customer',  customerRouter)
 app.use('/user', tcRouter)
 app.use('/user', previewRouter)
 app.use('/Quotation', qutation)
// app.use('/admin', selectProdcutRouter)
app.use('/pdf', pdf)

 app.use('/public/uploads', express.static('./public/uploads'));
 app.use( express.static('./pdf'));
 

 const swaggerDefinition = {
  info: {
    title: "EYEMATIC APP",
    version: "1.0.0",
    description: "Backend of eyematic app",
  },
  host: `192.168.1.37:3000`,
  basePath: "/",
};

const options = {
  swaggerDefinition: swaggerDefinition,
  apis: ["./router/*.js"],
};

const swaggerSpec = swaggerJSDoc(options);
app.get("/swagger.json", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerSpec);
});
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));
 app.listen(port,()=>{
    console.log(`server running port  ${port}`)
 });
 