const { urlencoded } = require('express');
const mongoose = require('mongoose');
const schema = require('mongoose').Schema
const mongoosePaginate = require('mongoose-paginate');
const productSchema = new schema({
userId:{
   type: mongoose.Schema.Types.ObjectId, ref:"ADMIN"
},
productName:{
 type:String, require:true
},
price:{
	type:Number, require:true
},
quantity:{
	type:Number, require:true, default: 1, 
},
discount:{
	type:Number, require:true
},
gst:{
	type:Number, require:true
},
image:{
	type: String, require: true,

},
date:{
	type:Date, require:true,
	
	default:Date.now()
},
totalAmounts:{
type: Number
},
userType:{
	type:String, enum:["ADMIN", "USER"],default:"USER"
	
},
status:{type:String, enum:["ACTIVE", "DELETE"], default:"ACTIVE"}
},
{
	timestamps:true
})

productSchema.plugin(mongoosePaginate)
const productModel = mongoose.model("product", productSchema)
module.exports= productModel;
// function getNextSequenceValue(sequenceName){
// 	var sequenceDocument = db.counters.findAndModify({
// 	   query:{_id: sequenceName },
// 	   update: {$inc:{sequence_value:1}},
// 	   new:true
// 	});
// 	return sequenceDocument.sequence_value;
//  }