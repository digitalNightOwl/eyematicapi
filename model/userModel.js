const mongoose = require('mongoose');
const bcrypt = require("bcryptjs");
const validator = require('validator')
const schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const userSchema = new schema({
    userId:{
        type: mongoose.Schema.Types.ObjectId, ref:"USER"
     },
     
    name:{
        type:String, require:true,
        maxlength: 50
    },
    country:{type:String, require:true,
    maxlength: 100     
    },  
    mobileNumber:{
       type: String, require:true,
      
    },
    companyName:{
        type: String, require: true,
    },
    city:{
        type: String, require: true,
    },
     pinCode:{
        type: String, require: true,
     },
    gst:{
    type: String, require: true,
}, 
	 email:{
        type:String, require:true,
        trim: true,
        unique: 1
    },
    website:{
        type: String, require: true,
    },
    password:{
        type:String, require:true,
    },   
    otp:{
    type:Number, require:true,
},
   expTime:{
    type:String, require: true,
},
  otpvarification:{
    type:Boolean, require:true,
    default:false
},
image:{
    type:String, require:true
},
userType:{
    type: String,
    enum:["ADMIN","USER"],
    default: "USER"
},
status:{
    type:String,
    enum:["ACTIVE", "BLOCK","DELETE",],
    default:"ACTIVE"
}, 
},
{timestamps:true}

);
userSchema.plugin(mongoosePaginate)
    const userModel = mongoose.model("user", userSchema);
    module.exports = userModel; 
    userModel.findOne({status:{$ne:"DELETE"},userType:"ADMIN"},(err, adminResult)=>{
        if (err) {
            console.log("Admin creation error",err)
        } else if (adminResult) {
            console.log('default admin exist,')
  }
 else {
            let admin ={
                companyName:"Greenusys Technology",
                mobileNumber:"8009652104",
                address:"Noida",
                email:"admin@gmail.com",
                website:"https://greenusys.com/",
                gst:"33AXCPS5117M1ZA",
                pincode:"201309",
                password:bcrypt.hashSync("1000"),
                userType:"ADMIN"
            }
            userModel(admin).save((error, adminCreate)=>{
                if (error) {
                console.log("Admin creation error:",error)                    
                } else {
                    console.log('Default admin created')   
                }
            })
    }
})

    