const mongoose = require('mongoose')
const schema = require('mongoose').Schema
const staticSchema = new schema({
    title:{type:String, require:true},
    email:{type:String,  require:true},
    phone:{type:String,  require: true},
    address:{type:String, require:true},
    website:{type:String,  require:true},     
    userType:{
      type:String, enum:["ADMIN", "USER"],default:"ADMIN"     
    },
    status:{
      type:String, enum:["ACTIVE", "BLOCK"], defualt:"ACTIVE"
    }
})
const staticModel = mongoose.model("admin", staticSchema)
module.exports = staticModel
staticModel.findOne({userType:{$in:"ADMIN"}},(err, res)=>{
  if(err){
    console.log("server error");
  } 
  else if(res){
    console.log("content is already exist ");
  } 
  else{
    let admin={
        title:"Contact Us",
        email:"greenusys@gmail.com",
        phone:"8978787870",
        address:"Noida",
        website: "https://greenusys.com/",
    }
    staticModel(admin).save((error, adminCreate)=>{
      if (error) {
      console.log("static creation error:",error)                    
      } else {
          console.log('Default static created')   
      }
  })
  }
})