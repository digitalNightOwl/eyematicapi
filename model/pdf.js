const express= require('express');
const mongoose = require('mongoose');
const schema = require('mongoose').Schema
const mongoosePaginate = require('mongoose-paginate');
const pdfSchema = new schema({
userId:{ 
	
   type: mongoose.Schema.Types.ObjectId, ref:"USER"
},
productName:{type:String, require:true},
companyName:{
 type:String, require:true
},
customer:{
    type:String, require:true
},
billId:{
    type:String,
    require:true
},
pdflink:{
    type:String,
    require:true
},


},
{
	timestamps:true
}
)
pdfSchema.plugin(mongoosePaginate)
const pdfModel = mongoose.model("pdf",pdfSchema)
module.exports= pdfModel;
