const mongoose = require('mongoose')

const schema = mongoose.Schema
 const selectProductSchema = new schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "USER"
  },
  products: [
    {
      productId: Number,
      quantity: Number,
      name: String,
      price: Number
    }
  ],
      active: {
        type: Boolean,
        default: true
      },
      modifiedOn: {
        type: Date,
        default: Date.now()
      }
  },
  { timestamps: true }
  );
  const selectProductModel=mongoose.model("newQuotation", selectProductSchema)
  module.exports = selectProductModel
  
