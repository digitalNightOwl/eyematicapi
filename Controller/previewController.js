const previewModel = require('../model/previewModel')
const productModel = require("../model/addProduct")
const userModel = require("../model/userModel")
const companyModel = require('../model/addComanyModel')
const customerModel = require('../model/customerAddModel')
const tcModel = require('../model/tcModel')
const idAutoIncrement = require('id-auto-increment')
module.exports = {
  previewData: async (req, res) => {
    try {  
      const{quotation} = req.body
      const user = await userModel.findOne({ userType: "USER" })  
      
      if (!user) {
        return res.json({ responseCode: 403, responseMessage: "data not found" })
      }
      else {
        const customer = await customerModel.findOne({_id: req.query.customerId, userType:"USER",  status:"ACTIVE" },)
        if (!customer) {
          return res.json({ responseCode: 403, responseMessage:"customer data not found" })
        }else {
       const company = await companyModel.findOne({_id: req.query.companyId, userType:"USER", status:"ACTIVE"})
           if (!company) {
            return res.send({responseCode: 403, responseMessage:"company Data not found"})
           } else {
          const product = await productModel.findOne({_id: req.query.productId, userType:"USER", status:"ACTIVE"})  
          if (!product) {
  
            return res.send({responseCode:403, responseMessage:"product data not found"})
           } else {
            const tc = await tcModel.findOne({_id: req.query.tcId, userType:"USER"})
            if (!tc) {
             return res.send({responseCode: 403, responseMessage:" tc data not found"})
            } else {
              const preview = await previewModel.findOne({_id: req.body.userId})
             if (preview) {
              return res.send({responseCode: 403, responseMessage:"data akredy exist"})
             } else {
              let data = previewModel({
                userId: req.query.userId,
                customerId: req.body.customerId,
                companyId: req.body.companyId,
                tcId: req.body.tcId,
                productId: req.body.productId
              })
              const save = await previewModel(data).save()
              return res.send({responseCode:200, responseMessage:"preview successfully", resposneResult: [save, company, product, customer, tc]})
             }
               
              }      
              }
      
          }
        
      }
     
    }
  
    }
    catch (error) {
      return res.json({ responseCode: 501, responseMessage: "something went wrong", })
    }
  },
  viewPreviewData : async (req, res)=>{
    try {
      const user = await previewModel.findOne({_id: req.query._id, userType:"ADMIN"})
      if (!user) {
        return res.send({responseCode:403, responseMessage:"Preview data not found"})

      } else {
        res.send({responseCode:200, responseMessage:"preview successfully", resposneResult:user})
        
      }
    } catch (error) {
       return res.send({responseCode:501, responseMessage:"something went wrong", resposneResult:Message.error})    
    }
  }
}