const userModel = require('../model/userModel');
const productModel= require("../model/addProduct")
const responseCode = require('../responseCode')
const responseMessage = require('../responseMessage')
module.exports = {
	addProduct: async (req, res) => {
		try {
			const{productName, price, quantity, discount, gst ,userId,  image } = req.body
			// console.log("============================>", req.body)
			const user = await userModel.findOne({ _id: req.body.userId})
			var totalprice = price*quantity;
			var discountPercent = (totalprice/100)*discount
			var total = totalprice-discountPercent;
			var addGst =  (total/100)*gst;
			var totalAmount = total + addGst
			if (!user) {
				return res.json({ responseCode:403, responseMessage:"user  data not found"})	
				
			} else {	
				// const url =  `http://192.168.1.26:3000/public/uploads/${req.file.filename}`;
				const product = await productModel.findOne({_id: req.body.userId})
				if (product) {
					return res.send({responseCode:403, responseMessage:"product alredy exist"})
				} else {
									
					const object = productModel ({   
						userId: userId,
						productName: productName,
                        price: price,
						quantity:quantity,			
                        discount: discount,
                        gst:gst,
						totalAmounts: totalAmount,
						image : image
						
					})
					const productSave = await productModel(object).save()					
					return res.json({ responseCode: responseCode.SUCCESS, responseMessage: responseMessage.PRODUCT_ADD, responseSucess:"true", responseResult:productSave ,  })
				}
			}
		} catch (error) {
			return res.json({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG })
		}
	},

	productList: async (req, res) => {
		try {
			const list = await productModel.find({ userId: req.query.userId})
			if (!list) {
				return res.json({ responseCode: responseCode.DATA_NOT_FOUND, responseMessage: responseMessage.DATA_NOT_FOUND , })

			} else {
				return res.json({ responseCode: 200, responseMessage: responseMessage.PRODUCT_LIST, responseSuccess:"true", responseResult: list })
			}
		} catch (error) {
			return res.json({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG })
		}
	},
	editProductData: async (req, res) => {
		try {
			const{productName, price, quantity, discount, gst, image} = req.body
			const editData = await productModel.findOne({_id: req.query.productId,  userType: "USER", })
			if (!editData) {
				return res.json({ responseCode: responseCode.DATA_NOT_FOUND, responseMessage: responseMessage.DATA_NOT_FOUND })

			} else {
				let dataEdit = await productModel.findByIdAndUpdate({ _id: editData.productId }, { $set: {productName: productName, price: price, quantity: quantity, discount: discount, gst: gst, image:image}},{new:true})
				if (dataEdit) {
					return res.json({ responseCode: responseCode.SUCCESS, responseMessage: responseMessage.PRODUCT_EDIT,responseSuccess:"true", responseResult: dataEdit });
				}
			}
		} catch (error) {
			return res.json({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG })
		}
	},
	productDelete: async (req, res) => {
		try {
			const deleteData = await productModel.findOne({_id: req.query.productId })

			if (!deleteData) {
				return res.json({ responseCode: responseCode.DATA_NOT_FOUND, responseMessage: responseMessage.DATA_NOT_FOUND })


			} else {
				const data = await productModel.findByIdAndDelete({ _id: deleteData._id })
				return res.json({ responseCode: responseCode.SUCCESS, responseMessage: responseMessage.PRODUCT_DELETE, responseSuccess:"true"})
			}
		} catch (error) {
			console.log(error)
			res.send({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG })
		}
	},
	searchAPI: async(req, res)=>{
		try {
		    const search = req.body.search;
			const product = await productModel.find({"productName": {$regex: ".*" +search+ ".*"}})
			if(product.length>0){
				res.send({responseCode:responseCode.SUCCESS, responseMessage:responseMessage.SEARCH, responseResult:product})
			}
			else{
				res.send({responseCode:responseCode.DATA_NOT_FOUND, responsemessage:responseMessage.DATA_NOT_FOUND})
			}
		} catch (error) {
			res.send({responseCode:responseCode.SOMETHING_WRONG, responseMessage:responseMessage.SOMETHING_WRONG, });
		}
	},
	productDataView: async(req, res)=>{
		try {
				const product = await  productModel.findOne({_id: req.query._id })
		if(!product){
            res.send({responseCode:403, responseMessage:"data not found"})
		}else{
				res.send({responseCode:200, responseMessage:" product Data view successfully", responseResult:product})
		}
		} catch (error) {
				res.send({responseCode: 501, responseMessage:"Something went wrong"})
		}
},

  uploadImage:async(req,res)=>{
try{

	const {productId,image}=req.body
const product=await productModel.findOne({_id:productId})

if(!product){
res.send({responseCode:403, responseMessage:"data not found"})
}else{
const url = `http://192.168.1.37:3000/public/uploads/${req.file.filename}`;
const im=await productModel.findByIdAndUpdate({_id:productId},{ $set: { image:url } },{ new: true });
return res.send({ responseCode: 200, responseMessage: "upload image successfully", responsResult: im })
}

}catch(error){
return res.send({ responseCode: 501, responseMessage: "Something went wrong", responseResult: error.message })

}
	}
}
 

