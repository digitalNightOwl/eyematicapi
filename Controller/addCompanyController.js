
const userModel = require('../model/userModel')
const companyModel = require('../model/addComanyModel');
const responseCode = require('../responseCode')
const responseMessage = require('../responseMessage')
module.exports = {
	addCompany: async (req, res) => {
		try {
			const {image,  userId, companyName, personName, gstNumber, mobileNumber, email, address, city, pinCode, website } = req.body
			const user = await userModel.findOne({_id: req.body.userId })
			if (!user) {
				return res.json({ responseCode: responseCode.DATA_NOT_FOUND, responseMessage: responseMessage.DATA_NOT_FOUND })
			} else {
				var companyD = await companyModel.findOne({ _id: req.body.userId, userType:"USER"})
				if (companyD) {
					return res.json({ responseCode: responseCode.ALREADY_EXIST, responseMessage: responseMessage.ALREDY_EXIST })
				} else {
					let companyData = {
                        userId: userId,
						companyName: companyName,
						personName: personName,
						gstNumber: gstNumber,
						mobileNumber: mobileNumber,
						email: email,
						gstNumber: gstNumber,
						address: address,
						city: city,
						pinCode: pinCode,
						website: website,
						image : image
					}
					const companyAdd = await companyModel(companyData).save()
					return res.json({ responseCode: responseCode.SUCCESS, responseMessage: responseMessage.COMPANY_ADD, responseResult: companyAdd })
				}
			}
		} catch (error) {
			return res.json({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG })
		}
	},
	companyList: async (req, res) => {
		try {
			const list = await companyModel.find({ userId: req.query.userId})
	
			if (!list) {
				return res.json({ responseCode: responseCode.DATA_NOT_FOUND, responsemessage: responseMessage.DATA_NOT_FOUND })

			} else {
				return res.json({ responseCode: 200, responsemessage: responseMessage.LIST, responseSuccess: "true",  responseResult: list })
			}
		} catch (error) {
			console.log(error)
			return res.json({ responseCode: responseCode.SOMETHING_WRONG, responsemessage: responseMessage.SOMETHING_WRONG })
		}
	},
	editCompanyData: async (req, res) => {
		try {
			const { companyName, personName, gstNumber, mobileNumber, email, address, city, pinCode, website } = req.body
			const editData = await companyModel.findOne({_id: req.query.companyId,   })
			if (!editData) {
				return res.json({ responseCode: responseMessage.DATA_NOT_FOUND, responseMessage: responseMessage.DATA_NOT_FOUND })

			} else {
				let dataEdit = await companyModel.findByIdAndUpdate({ _id: editData._id }, {companyName: companyName, personName: personName,gstNumber:gstNumber, mobileNumber: mobileNumber, email: email, address: address, city: city, pinCode: pinCode, website: website },{new:true})

				if (dataEdit) {
					return res.json({ responseCode: responseCode.SUCCESS, responseMessage: responseMessage.COMPANY_EDIT, responseSuccess: "true", responseResult: dataEdit });
				}
			}
		} catch (error) {
			console.log(error)
			return res.json({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG })
		}
	},
	companyDelete: async (req, res) => {
		try {
			const deleteData = await companyModel.findOne({ _id: req.query.companyId,  })

			if (!deleteData) {
				return res.json({ responseCode: responseCode.DATA_NOT_FOUND, responsemessage: responseMessage.DATA_NOT_FOUND })


			} else {
				const data = await companyModel.findByIdAndDelete({ _id: deleteData._id })
				return res.json({ responseCode: responseCode.SUCCESS, responsemessage: responseMessage.DELETE, responseSuccess: "true", responseResult: deleteData})
			}
		} catch (error) {
			res.send({ responseCode: responseCode.SOMETHING_WRONG, responsemessage: responseMessage.SOMETHING_WRONG })
		}
	},
	companyDataView: async (req, res) => {
		try {
			const data = await companyModel.findOne({ _id: req.query._id, })

			if (!data) {
				res.send({ responseCode: responseCode.DATA_NOT_FOUND, responseMessage: responseMessage.ADMIN_NOT_FOUND, })

			} else {
				res.send({ responseCode: responseCode.SUCCESS, responseMessage: responseMessage.VIEW, responseResult: data })

			}
		} catch (error) {
			res.send({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG, responseResult: error.Message })
		}
	}
}
