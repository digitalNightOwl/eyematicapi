const userModel = require('../model/userModel')
const productModel = require('../model/addProduct')
const selectProductModel = require('../model/selectProductModel')
module.exports = {
    selectProduct: async (req, res)=>{
        try {
            const user = await userModel.findOne({_id: req.body.userId, userType:"ADMIN", status:"ACTIVE"})
            if (!user) {
                return res.send({resposneCode: 403, responseMessage:"Data not found"})
            } else {
                const product = await productModel.findOne({_id: req.body.productId, userType:"ADMIN",})
                if (!product) {
                    return res.send({resposneCode:403, responseMessage:"product data not found"})
                } else {
                    const selectProduct = await selectProductModel.findOne({productId: req.body.productId, userType:"ADMIN", status:"ACTIVE"})
                    if (selectProduct) {
                        if (req.body.quantity) {
                            const updateproduct = await selectProductModel.findByIdAndUpdate({_id: selectProduct._id},{quantity: req.body.quantity}, {new: true})
                            let total = await selectProductModel.findByIdAndUpdate({_id:selectProduct._id},{totalPrice: updateproduct.quantity* product},{new: true})
                       return res.send({resposneCode: 200, responseMessage:"select product successfully", responseResult:{updateproduct, product }})
                        } else {
                            const updateproduct = await selectProductModel.findByIdAndUpdate({_id: req.body.productId}).populate('product')
                        }
                    } else {
                        
                    }
                }
            }
        } catch (error) {
            
        }
    }
}