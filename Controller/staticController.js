const staticModel = require('../model/statticModel');
const responseCode = require('../responseCode')
 const responseMessage = require('../responseMessage')
 module.exports = ({
    view: async (req, res)=>{
        try {
            let admin = await staticModel.findOne({ userType:"ADMIN"})
            console.log("====================================>8", admin)
            if(!admin){
                res.send({ responseCode: responseCode.USER_NOT_FOUND, responseMessage: responseMessage.DATA_NOT_FOUND})

            }else{
                return res.send({ responseCode:responseCode.SUCCESS, responseMessage: responseMessage.STATIC_VIEW, responseResult: admin})
            }
        } catch (error) {
            res.send({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG})
        }
    },
    
     editStaticData: async (req, res) => {
        try {
            let admin = await staticModel.findOne({ _id: req.query._Id, status: "ACTIVE", userType: "ADMIN" })
            if (!admin) {
                res.send({ responseCode: responseCode.USER_NOT_FOUND, responseMessage: responseMessage.CONTENET, responseResult: [] })
            } else {
                let userAdd = await staticModel.findByIdAndUpdate({ _id: admin._id }, { $set: { title: req.body.title, phone: req.body.phone, address: req.body.address, website: req.body.website,email: req.body.email   } }, { new: true })
                if (userAdd) {
                    return res.send({ responseCode: responseCode.SUCCESS, responseMessage: responseMessage.STATIC_ADD, responseResult: userAdd })
                }
            }
        } catch (error) {
            res.send({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG, responseResult: error.message })
        }
    }
})
