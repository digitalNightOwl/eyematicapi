const tcModel = require("../model/tcModel")
const responseCode = require("../responseCode")
const responseMessage = require("../responseMessage")
module.exports = ({
    tcview: async (req, res) => {
        try {
            let admin = await tcModel.findOne({ _id: req.query.tcId,})
            console.log(admin)
            if (!admin) {
                res.send({ responseCode: responseCode.USER_NOT_FOUND, responseMessage: responseMessage.CONTENET, responseResult: [] })
            } else {
                return res.send({ responseCode: responseCode.SUCCESS, responseMessage: "Terms & condition view Successfully", responseResult: admin })
            }
        } catch (error) {
            res.send({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG, responseResult: error.message })
        }
    },
    // list: async (req, res) => {
    //     try {
    //         let admin = await tcModel.find({ status: "ACTIVE" })
    //         if (!admin) {
    //             res.send({ responseCode: responseCode.USER_NOT_FOUND, responseMessage: responseMessage.CONTENET, responseResult: [] })
    //         } else {
    //             return res.send({ responseCode: responseCode.SUCCESS, responseMessage: responseMessage.STATIC_LIST, responseResult: admin })
    //         }
    //     } catch (error) {
    //         res.send({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG, responseResult: error.message })
    //     }
    // },
    tceditData: async (req, res) => {
        try {
            // const{type}=req.body
            let admin = await tcModel.findOne({ userType: "USER" })
            if (!admin) {
                res.send({ responseCode: responseCode.USER_NOT_FOUND, responseMessage: responseMessage.CONTENET, responseResult: [] })
            } else {
                let userAdd = await tcModel.findByIdAndUpdate({ _id: admin._id }, { $set: { description: req.body.description,  } }, { new: true })
                if (userAdd) {
                    return res.send({ responseCode: responseCode.SUCCESS, responseMessage: "Terms & condition edit successfully", responseResult: userAdd })
                }
            }
        } catch (error) {
            res.send({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG, responseResult: error.message })
        }
    }
})