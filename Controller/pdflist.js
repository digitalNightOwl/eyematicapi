const pdf = require('../model/pdf')
module.exports = {
pdfList: async (req, res)=>{
    try {
        const data = await pdf.find()
    if (!data) {
        return res.send({responseCode:404, responseMessage:"no pdf list found" })
    } else {
        return res.send({responseCode:200, responseMessage:"pdf list find successfully", responseResult: data})
    }
    } catch (error) {
    console.log(error)
    }
},

showPdfById: async(req, res)=>{
    try {
        const data = await pdf.findOne({_id: req.query.pdfId})
        if (!data) {
            return res.send({responseCode:404, responseMessage:"pdf data not found"})
        } else {
           return res.send({responseCode:200, responseMessage:"pdf data show succesfully", responseResult:data}) 
        }
    } catch (error) {
        return res.send({responseCode:501, responseMessage:"Something went wrong"})
    }
},
deletePdfById: async(req, res)=>{
    try {
        const data =  await pdf.findOne({id: req.query.pdfId})
        if (!data) {
            return res.send({responseCode:404, responseMessage:"pdf data not found"})
        } else {
            const deltePdf = await pdf.findByIdAndDelete({_id: data._id})
            return res.send({responseCode:200, responseMessage:"pdf  delete Successfuly"})
        }
    } catch (error) {
 return response.send({responseCode:501, responseMessage:"Something went wrong"})       
    }
}
}