const express = require('express');
const userModel = require('../model/userModel')
const { query } = require('express');
const customerModel = require("../model/customerAddModel")
const responseCode = require('../responseCode');
const responseMessage = require('../responseMessage');
const status = require("../enums/status");
const userType = require('../enums/userType');
module.exports = {
	addCustomer: async (req, res) => {
		try {
			const{customerName, userId, companyName, address, email, zipCode, city, country, contactNumber} = req.body
			const addData = await userModel.findOne({_id: req.body.userId, })
			if (!addData) {
				return res.json({ responseCode: responseCode.DATA_NOT_FOUND, responseMessage: responseMessage.DATA_NOT_FOUND})	
				} else {
					const customer = await customerModel.findOne({_id: req.body.userId})
					if (customer) {
						return res.send({responseCode:403, responseMessage:"Alredy exist"})
					} else {					
					let customerData = customerModel({
					userId: userId,
			        customerName: customerName,
                    companyName: companyName,
                    address: address,
                    email:email,
                    zipCode: zipCode,
                    city: city,
                    country: country,
                    contactNumber: contactNumber,
					})
					const customerSave = await customerModel(customerData).save()  
					return res.json({ responseCode: responseCode.SUCCESS, responseMessage: responseMessage.CUSTOMER_ADD, responseSucess:"true",responseResult: customerSave })
				}

			}
		} catch (error) {
			return res.json({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG })
		}
	},
	customerList: async (req, res) => {
		try {
			const list = await customerModel.find({  _id: req.userId,})
			if (!list) {
				return res.json({ responseCode: responseCode.USER_NOT_FOUND, responsemessage: responseMessage.DATA_NOT_FOUND })

			} else {
				return res.json({ responseCode: 200, responsemessage: responseMessage.LIST, responseSucess:"true",responseResult: list })
			}
		} catch (error) {
			return res.json({ responseCode: responseCode.SOMETHING_WRONG, responsemessage: responseMessage.SOMETHING_WRONG })
		}
	},
	editCustomerData: async (req, res) => {
		try {
			const{customerName, companyName, address, email, zipCode, city, country, contactNumber} = req.body
			const editData = await customerModel.findOne({ _id: req.query._id , userType: "ADMIN", })
			if (!editData) {
				return res.json({ responseCode: responseMessage.USER_NOT_FOUND, responseMessage: responseMessage.DATA_NOT_FOUND })

			} else {
				let dataEdit = await customerModel.findByIdAndUpdate({ _id: editData._id }, {customerName: customerName, companyName: companyName, address:address, email: email, city: city, zipCode:zipCode, contactNumber: contactNumber, country: country},{new:true})
				if (dataEdit) {
					return res.json({ responseCode: responseCode.SUCCESS, responseMessage: responseMessage.DATA_EDIT, responseSucess:"true",responseResult: dataEdit });
				}
			}
		} catch (error) {
			return res.json({ responseCode: responseCode.SOMETHING_WRONG, responseMessage: responseMessage.SOMETHING_WRONG })
		}
	},
	customerDelete: async (req, res) => {
		try {
			const deleteData = await customerModel.findOne({ _id: req.query._id, userType:"ADMIN" })

			if (!deleteData) {
				return res.json({ responseCode: responseCode.USER_NOT_FOUND, responsemessage: responseMessage.DATA_NOT_FOUND })


			} else {
				const data = await customerModel.findByIdAndDelete({ _id: deleteData._id })
				return res.json({ responseCode: responseCode.SUCCESS, responsemessage: responseMessage.DELETE, responseSucess:"true",responseResult: data })
			}
		} catch (error) {
			res.send({ responseCode: responseCode.SOMETHING_WRONG, responsemessage: responseMessage.SOMETHING_WRONG })
		}
	},
    customerBlock: async (req, res) => {
		try {
			const blockData = await customerModel.findOne({ _id: req.query._id})

			if (!blockData) {
				return res.json({ responseCode: responseCode.USER_NOT_FOUND, responsemessage: responseMessage.DATA_NOT_FOUND })
			} else {
				const data = await customerModel.findByIdAndUpdate({ _id: req.query._id }, { $set: { status: "BLOCK" } }, { new: true })
				return res.json({ responseCode: responseCode.SUCCESS, responsemessage: responseMessage.CUSTOMER_BLOCK, responseSucess:"true", responseResult: data })
			}
		} catch (error) {
			res.send({ responseCode: responseCode.SOMETHING_WRONG, responsemessage: responseMessage.SOMETHING_WRONG })
		}
	},
	viewCoustomerData: async(req, res)=>{
		try {
			const customer = await customerModel.findOne({_id: req. query._id, userType:"ADMIN"})
			if (!customer) {
	     res.send({responseCode:403, responseMessage:"Data not found"})			
			} else {
				res.send({responseCode:200, responseMessage:"customer data view successfully", responseResult: customer})
			}
		} catch (error) {
			res.send({responseCode:501, responseMessage:"something went wrong"})
		}
	
	}
}