const fs = require("fs");
const PDFDocument = require("pdfkit");
const pdfModel=require('../model/pdf')
const companyModel = require('../model/addComanyModel');
const customerModel = require("../model/customerAddModel")
const productModel= require("../model/addProduct")
const crypto = require("crypto");
var path = require('path');

exports. createInvoice=async(req,res) =>{

  try {
    

    let doc = new PDFDocument({ size: "A4", margin: 50 });

    const {userId,customerId,companyId,productId}=req.body
    
    var company = await companyModel.findOne({ _id: companyId, userType:"USER"})
    var customer=await customerModel.findOne({_id:customerId})
    
     
    var distance=215;

        doc
          .image("logo2.png", 30, 20, { width: 100 })
          .fillColor("#444444")
          .fontSize(20)
          .text("Quotation", 250, 57)
          .fontSize(13)
          .text(company.companyName, 275, 87)
          .text(`${company.email} | ${company.mobileNumber}`, distance, 100)
          .text(`${company.address}| ${company.pinCode}`, distance-15, 115)
          
          .moveDown();
          console.log((company.email).length)
          console.log(distance +(company.email).length+80)
          
        doc
              .fillColor("#444444")
              .fontSize(20)
              .text("Bill", 50, 160);
          
            generateHr(doc, 185);
          
            const customerInformationTop = 200;
          const billid= crypto.randomBytes(16).toString("hex");
            doc
              .fontSize(10)
              .text("Bill Id:", 50, customerInformationTop)
              .font("Helvetica-Bold")
              .text(billid, 150, customerInformationTop)
              .font("Helvetica")
              .text("Bill Date:", 50, customerInformationTop + 15)
              .text(formatDate(new Date()), 150, customerInformationTop + 15)
              
          
              .font("Helvetica-Bold")
              .text(customer.customerName, 350, customerInformationTop)
              .font("Helvetica")
              .text(customer.address, 350, customerInformationTop + 15)
              .text(
                customer.city +
                  ", " +
                  customer.zipCode,
                350,
                customerInformationTop + 30
              )
              .moveDown();
          
            generateHr(doc, 252);
        
      let i;
      const invoiceTableTop = 330;
    
      doc.font("Helvetica-Bold");
      generateTableRow(
        doc,
        invoiceTableTop,
        "Product Name",
        "Price",
        "Quantity",
        "Total Amount"
      );
      generateHr(doc, invoiceTableTop + 20);
      doc.font("Helvetica");
      var total=0;

      for (i = 0; i < productId.length; i++) {
      console.log(productId.length)

        var product=await  productModel.findOne({_id:productId[i]})
        const position = invoiceTableTop + (i + 1) * 30;
        var totalPrice=product.price * product.quantity
        generateTableRow(
          doc,
          position,
          product.productName,
          product.price ,
          product.quantity,
          totalPrice
        );
    
        generateHr(doc, position + 20);
         total= total + totalPrice
        console.log("total",total)
      }
    
      const subtotalPosition = invoiceTableTop + (i + 1) * 30;



      generateTableRow(
        doc,
        subtotalPosition,
        "",
        "",
        "Total" ,
        
        total
      );
    
      doc.font("Helvetica");
    
        doc.fontSize(15)
        .text("Terms & Conditions", 50,500)
          .fontSize(10)
          .text(
            "This Company is headed up by Mr. Shahid Hakim, who have a wealth of knowledge and expereince in Loss Prevention and CCTV security systems",
            50,
            540,
            // { align: "center", width: 500 }
          );
          generateHr(doc, 500 + 20);
    
    
    var path2="invoice.pdf"
    
    
      doc.end();
      var filename=`${Date.now()}_${path2}`
      doc.pipe(fs.createWriteStream(`${__dirname}/../pdf/${filename}`));
     
     var pdff=`http://3.111.170.44:4000/${filename}`
    console.log(pdff)



     const doc1 = await pdfModel({ 
      userId:userId, 
      companyName: company.companyName,
      productName: product.productName,
      customer: customer.customerName,
      billId: billid,
      pdflink: pdff, 
  }).save();               
  // var oldPassword = hashPassword
  const pdfSave = await pdfModel(doc1).save()   




     res.json({responseCode:200, message:"pdf shown successfully", data:pdfSave})


  } catch (error) {
    res.send({error:error})
    console.log(error)
  }
  
    }



function generateTableRow(
  doc,
  y,
  item,
  description,
  unitCost,
  quantity,
  lineTotal
) {
  doc
    .fontSize(10)
    .text(item, 50, y)
    .text(description, 150, y)
    .text(unitCost, 280, y, { width: 90, align: "right" })
    .text(quantity, 370, y, { width: 90, align: "right" })
    .text(lineTotal, 0, y, { align: "right" });
}

function generateHr(doc, y) {
  doc
    .strokeColor("#aaaaaa")
    .lineWidth(1)
    .moveTo(50, y)
    .lineTo(550, y)
    .stroke();
}



function formatDate(date) {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return year + "/" + month + "/" + day;
}

