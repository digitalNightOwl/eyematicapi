const router=require('express').Router()
const tcRouter = require('../Controller/tcController')
/**
 * @swagger
 * /user/tcview:
 *   get:
 *     tags:
 *       - TERM CONDITION
 *     description: tcview
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token required.
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: tc View Successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */
router.get('/tcview',tcRouter. tcview)
// /**
//  * @swagger
//  * /user/list:
//  *   get:
//  *     tags:
//  *       - TERM CONDITION
//  *     description: Static list
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: token
//  *         description: token required.
//  *         in: query
//  *         required: true
//  *     responses:
//  *       200:
//  *         description: Static list.
//  *       500:
//  *         description: Internal Server Error
//  *       501:
//  *         description: Something went wrong!
//  */

// router.get("/list",tcRouter.list)
/**
 * @swagger
 * /admin/tceditData:
 *   post:
 *     tags:
 *       - TERM CONDITION
 *     description: tceditData
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token required.
 *         in: header
 *         required: true
 *       - name: _id
 *         description: _id required.
 *         in: query
 *         required: true
 *       - name: description
 *         description: description required.
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description:  edit successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */
router.post("/tceditData",tcRouter.tceditData)
module.exports=router