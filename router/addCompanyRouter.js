const express =require('express')
const router = express.Router();
const auth = require('../middleware/auth')
const companyController = require("../Controller/addCompanyController")

 /**
 * @swagger
 * /company/addCompany:
 *   post:
 *     tags:
 *       - ADD NEW COMPANY
 *     description: addCompany
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: userId required.
 *         in: formData
 *         required: true
 *       - name: companyName
 *         description: companyName required.
 *         in: formData
 *         required: true
 *       - name: personName
 *         description: personName required.
 *         in: formData
 *         required: true
 *       - name: gstNumber
 *         description: gstNumber required.
 *         in: formData
 *         required: true
 *       - name: mobileNumber
 *         description: mobileNumber required.
 *         in: formData
 *         required: true
 *       - name: email
 *         description: email required.
 *         in: formData
 *         required: true   
 *       - name: address
 *         description: address required.
 *         in: formData
 *         required: true
 *       - name: city
 *         description: city required.
 *         in: formData
 *         required: true
 *       - name: pinCode
 *         description: pinCode required.
 *         in: formData
 *         required: true
 *       - name: website
 *         description: website required.
 *         in: formData
 *         required: true
 *       - name: image
 *         description: image required.
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Thanks, company add successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.post('/addCompany',companyController.addCompany)
/**
 * @swagger
 * /company/companyList:
 *   get:
 *     tags:
 *       - ADD NEW COMPANY 
 *     description: companyList
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: userId required.
 *         in: query
 *         required: true
 *     responses:
 *       200:
 *         description: Thanks,  company list view successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.get('/companyList', companyController.companyList)
/**
 * @swagger
 * /company/editCompanyData:
 *   post:
 *     tags:
 *       - ADD NEW COMPANY
 *     description: editCompanyData
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: companyId
 *         description: companyId required.
 *         in: query
 *         required: true
 *       - name: companyName
 *         description: companyName required.
 *         in: formData
 *         required: true
 *       - name: personName
 *         description: personName required.
 *         in: formData
 *         required: true
 *       - name: gstNumber
 *         description: gstNumber required.
 *         in: formData
 *         required: true
 *       - name: mobileNumber
 *         description: mobileNumber required.
 *         in: formData
 *         required: true
 *       - name: email
 *         description: email required.
 *         in: formData
 *         required: true   
 *       - name: address
 *         description: address required.
 *         in: formData
 *         required: true
 *       - name: city
 *         description: city required.
 *         in: formData
 *         required: true
 *       - name: pinCode
 *         description: pinCode required.
 *         in: formData
 *         required: true
 *       - name: website
 *         description: website required.
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Thanks, company data edit  successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.post('/editCompanyData',companyController.editCompanyData)
/**
 * @swagger
 * /company/companyDelete:
 *   get:
 *     tags:
 *       - ADD NEW COMPANY 
 *     description: company
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: companyId
 *         description: companyId required.
 *         in: query
 *         required: true      
 *     responses:
 *       200:
 *         description: Thanks,  data delete  successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.get('/companyDelete', companyController.companyDelete)
/**
 * @swagger
 * /company/companyDataView:
 *   get:
 *     tags:
 *       - ADD NEW COMPANY 
 *     description: companyDataView
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: companyId
 *         description: _id required.
 *         in: query
 *         required: true
 *     responses:
 *       200:
 *         description: Thanks, view successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.get('/companyDataView',companyController.companyDataView)
module.exports=router;
