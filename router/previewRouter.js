const express = require('express')
 const router = express.Router()
 const auth = require('../middleware/auth')
 const previewController = require('../Controller/previewController')
 
/**
 * @swagger
 * /user/previewData:
 *   get:
 *     tags:
 *       - PREVIEW
 *     description: previewData
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: userId required.
 *         in: query
 *         required: true
 *       - name: customerId
 *         description: customerId required.
 *         in: query
 *         required: true 
 *       - name: companyId
 *         description: compnayId required.
 *         in: query
 *         required: true 
 *       - name: productId
 *         description: productId required.
 *         in: query
 *         required: true 
 *       - name: tcId
 *         description: tcId required.
 *         in: query
 *         required: true   
 *     responses:
 *       200:
 *         description: Thanks,  data preview successfully  .
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
 router.get('/previewData', previewController.previewData)
 /**
 * @swagger
 * /admin/viewPreviewData:
 *   get:
 *     tags:
 *       - PREVIEW
 *     description: viewPreviewData
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token required.
 *         in: header
 *         required: true
 *       - name: _id
 *         description: _id required.
 *         in: query
 *         required: true
 *     responses:
 *       200:
 *         description: Thanks, view successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.get('/viewPreviewData',previewController.viewPreviewData )
 module.exports = router; 