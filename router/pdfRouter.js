const express = require('express')
const router = express.Router()
const Controller = require('../Controller/pdflist')
/**
   * @swagger
   * /pdf/pdfList:
   *   get:
   *     tags:
   *       - pdf
   *     description: pdfList
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Thanks, You have successfully viewData.
   *       500:
   *         description: Internal Server Error
   *       501:
   *         description: Something went wrong!
   */  
router.get('/pdfList', Controller.pdfList)
/**
   * @swagger
   * /pdf/showPdfById:
   *   get:
   *     tags:
   *       - pdf
   *     description: showPdfById
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: pdfId
   *         description: pdfId required.
   *         in: query
   *         required: true
   *     responses:
   *       200:
   *         description: Thanks, You have successfully viewData.
   *       500:
   *         description: Internal Server Error
   *       501:
   *         description: Something went wrong!
   */  
router.get('/showPdfById', Controller.showPdfById)
/**
   * @swagger
   * /pdf/deletePdfById:
   *   delete:
   *     tags:
   *       - pdf
   *     description: deletePdfById
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: pdfId
   *         description: pdfId required.
   *         in: query
   *         required: true
   *     responses:
   *       200:
   *         description: Thanks, You have successfully viewData.
   *       500:
   *         description: Internal Server Error
   *       501:
   *         description: Something went wrong!
   */  
router.delete('/deletePdfById', Controller.deletePdfById)
module.exports = router;