const router=require('express').Router()
const  staticController = require('../Controller/staticController')
/**
 * @swagger
 * /admin/view:
 *   get:
 *     tags:
 *       - Contact Us
 *     description: view
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token required.
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description:  data view successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */
router.get('/view',staticController.view)
// /**
//  * @swagger
//  * /admin/list:
//  *   get:
//  *     tags:
//  *       - Static
//  *     description: Static list
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: token
//  *         description: token required.
//  *         in: header
//  *         required: true
//  *     responses:
//  *       200:
//  *         description: Static list.
//  *       500:
//  *         description: Internal Server Error
//  *       501:
//  *         description: Something went wrong!
//  */
// router.get("/list",staticController.list)
/**
 * @swagger
 * /admin/editStaticData:
 *   post:
 *     tags:
 *       - Contact Us
 *     description: editStaticData
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: _id
 *         description: _id required.
 *         in: query
 *         required: true
 *       - name: title
 *         description: title required.
 *         in: formaData
 *         required: true
 *       - name: email
 *         description: email required.
 *         in: formData
 *         required: true
 *       - name: phone
 *         description: phone required.
 *         in: formData
 *         required: true
 *       - name: address
 *         description: address required.
 *         in: formData
 *         required: true
 *       - name: website
 *         description: website required.
 *         in: query
 *         required: true
 *     responses:
 *       200:
 *         description: Static add.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */
router.post("/editStaticData",staticController.editStaticData)
module.exports=router;