const express =require('express');
const router = express.Router();
const bodyParser = require('body-parser')
const auth = require('../middleware/auth')
const productController = require("../Controller/addProductController")
const path  = require('path')
const multer = require('multer')
const { filter } = require('lodash');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended:true}));
router.use(express.static('public'));
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/uploads')
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
})
var upload = multer({ storage: storage , filter: filter})
/**
 * @swagger
 * /product/addProduct:
 *   post:
 *     tags:
 *       - ADD NEW PRODUCT
 *     description: addProduct
 *     produces:
 *       - multipart/form-data
 *     parameters: 
 *       - name: userId
 *         description: userId required.
 *         in: formData
 *         required: true
 *       - name: productName
 *         description: productName required.
 *         in: formData
 *         required: true
 *       - name: price
 *         description: price required.
 *         in: formData
 *         required: true
 *       - name: quantity
 *         description: quantity required.
 *         in: formData
 *         required: true
 *       - name: discount
 *         description: discount required.
 *         in: formData
 *         required: true
 *       - name: gst
 *         description: gst required.
 *         in: formData
 *         required: true 
 *       - name: image
 *         description: image required.
 *         in: formData
 *         required: true   
 *     responses:
 *       200:
 *         description: Thanks, product add successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.post('/addProduct', productController.addProduct)
/**
 * @swagger
 * /product/productList:
 *   get:
 *     tags:
 *       - ADD NEW PRODUCT
 *     description: companyList
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: userId required.
 *         in: query
 *         required: true
 *     responses:
 *       200:
 *         description: Thanks,  product list view successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.get('/productList', productController.productList)
/**
 * @swagger
 * /product/editProductData:
 *   post:
 *     tags:
 *       - ADD NEW PRODUCT
 *     description: editProductData
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: productId
 *         description: productId required.
 *         in: query
 *         required: true
 *       - name: productName
 *         description: productName required.
 *         in: formData
 *         required: true
 *       - name: price
 *         description: price required.
 *         in: formData
 *         required: true
 *       - name: quantity
 *         description: quantity required.
 *         in: formData
 *         required: true
 *       - name: discount
 *         description: discount required.
 *         in: formData
 *         required: true
 *       - name: gst
 *         description: gst required.
 *         in: formData
 *         required: true   
 *     responses:
 *       200:
 *         description: Thanks, product data edit  successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.post('/editProductData',auth.verifyToken, productController.editProductData)
/**
 * @swagger
 * /product/productDelete:
 *   get:
 *     tags:
 *       - ADD NEW PRODUCT
 *     description: productDelete
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: productId
 *         description: productId required.
 *         in: query
 *         required: true      
 *     responses:
 *       200:
 *         description: Thanks,  product delete  successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.get('/productDelete',productController.productDelete) 
/**
 * @swagger
 * /product/searchAPI:
 *   post:
 *     tags:
 *       - ADD NEW PRODUCT
 *     description: productDelete
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token required.
 *         in: header
 *         required: true  
 *       - name: search
 *         description: search required.
 *         in: formData
 *         required: true      
 *     responses:
 *       200:
 *         description: Thanks,  search successfully successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.post('/searchAPI', productController.searchAPI) ;
/**
 * @swagger
 * /product/productDataView:
 *   get:
 *     tags:
 *       - ADD NEW PRODUCT 
 *     description: productDataView
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: _id
 *         description: _id required.
 *         in: query
 *         required: true
 *     responses:
 *       200:
 *         description: Thanks, view successfully.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.get('/productDataView',productController.productDataView)
/**
 * @swagger
 * /product/uploadImage:
 *   post:
 *     tags:
 *       - ADD NEW PRODUCT 
 *     description: uploadImage
 *     produces:
 *       - multipart/form-data
 *     parameters: 
 *       - name: productId
 *         description: productId required.
 *         in: formData
 *         required: true
 *       - name: image
 *         description: image required.
 *         in: formData
 *         format: binary
 *         type: file
 *         required: ture
 *     responses:
 *       200:
 *         description: Thanks, You have successfully upload image.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */  
router.post('/uploadImage',upload.single('image'),productController.uploadImage)


module.exports=router;
